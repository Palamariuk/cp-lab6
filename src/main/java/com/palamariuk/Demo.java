package com.palamariuk;

import com.palamariuk.library.Book;
import com.palamariuk.library.Library;
import com.palamariuk.library.LibraryReaderInfo;
import com.palamariuk.library.LibrarySubscription;

import java.io.*;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.IntStream;

/*
Варіант1.
	Створити класи для бібліотеки. Клас Книга повинен містити інформацію про автора, назву та рік видання.
	Клас Абонемент знатиме прізвище, ім’я та по-батькові, а також електронну адресу.
	Кожен читач може взяти певну кількість книг, список яких він зберігає.
	Адміністратор фіксує дату, коли читач забирає книгу і дату планового повернення, а також дату реального повернення.
	Клас бібліотека містить колекцію всіх книг та колекцію всіх абонементів, а також методи, які з цими колекціями можна здійснювати.
Завдання.
	1. Відсортувати всі книги за роком видання.
	2. Створити список адресів для розсилки повідомлень для читачів, що взяли більше ніж 2 книги.
	3. Перевірити, скільки користувачів взяли книги заданого автора.
	4. Знайти найбільшу кількість книг, взятого читачем бібліотеки.
	5. Здійснити різну розсилку 2 групам користувачів. У першу групу занести користувачів, які мають менше 2 книг.
	Їм повідомити про новинки бібліотеки. У другу групу занести інших користувачів. Їм повідомити про вчасність повернення книг.
	6. Створити список боржників на біжучу дату. Для кожного боржника вказати список книг,
	які підлягають поверненню, з вказівкою кількості днів порушення терміну.
* */

public class Demo {

    private static final Scanner scanner = new Scanner(System.in);

    public static void task1(Library library) {
        library
                .getBooks()
                .stream()
                .sorted(Comparator.comparingInt(Book::getPublicationYear))
                .forEach(System.out::println);
    }

    public static void task2(Library library) {
        library
                .getLibrarySubscriptions()
                .stream()
                .filter(x -> x.getReader().getTakenBooks().size() > 2)
                .map(LibrarySubscription::getEmail)
                .forEach(System.out::println);
    }

    public static void task3(Library library) {
        long result = library
                .getLibrarySubscriptions()
                .stream()
                .filter(x -> x
                        .getReader()
                        .getTakenBooks()
                        .stream()
                        .map(Book::getAuthor)
                        .toList()
                        .contains("Card"))
                .distinct()
                .count();

        System.out.println(result);
    }

    public static void task4(Library library) {
        Optional<Integer> result = library
                .getLibrarySubscriptions()
                .stream()
                .map(x -> x.getReader().getTakenBooks().size())
                .max(Integer::compareTo);

        result.ifPresent(System.out::println);
    }

    public static void task5(Library library) {
        library
                .getLibrarySubscriptions()
                .stream()
                .filter(reader -> reader.getReader().getTakenBooks().size() >= 2)
                .forEach(reader -> reader
                        .getReader()
                        .getTakenBooks()
                        .forEach(book -> System.out.println(
                                reader.getName() + " " + reader.getSurname() + ", return " + book.getName() + " before "
                                        + library.getAdministrator().getLatestByBook(book).getPlannedReturnDate())));

        library
                .getLibrarySubscriptions()
                .stream()
                .filter(reader -> reader.getReader().getTakenBooks().size() < 2)
                .forEach(reader -> System.out.println(
                        reader.getName() + " " + reader.getSurname() + " we have a lot of books! For example: " + library.getBooks().subList(0, 3)));
    }

    public static void task6(Library library) {
        IntStream
                .range(1, 8)
                .mapToObj(day -> LocalDate.of(2021, 12, day))
                .forEach(date -> {
                    System.out.println("Today is " + date);
                    library
                            .getAdministrator()
                            .getRecords()
                            .stream()
                            .filter(x -> date.isAfter(x.getPlannedReturnDate()) &&
                                    (x.getActualReturnDate() == null || date.isBefore(x.getActualReturnDate())))
                            .forEach(x -> System.out.println(x.getLibrarySubscription().getName() + " " + x.getLibrarySubscription().getSurname() + " you delay returning of '" + x.getBook().getName()
                                    + "' for " + (date.getDayOfMonth() - x.getPlannedReturnDate().getDayOfMonth()) + " days"));
                });

    }

    public static void main(String[] args) throws Exception {
        Library library = new Library();
        fillLibraryWithBooks(library);
        fillLibraryWithSubscriptions(library);

        while (true) {
            System.out.print("Enter option: ");
            switch (scanner.nextInt()) {
                case 0 -> throw new Exception("End of execution.");
                case 1 -> task1(library);
                case 2 -> task2(library);
                case 3 -> task3(library);
                case 4 -> task4(library);
                case 5 -> task5(library);
                case 6 -> task6(library);
                case 7 -> serializingDemo(library);
                default -> System.out.println("Enter any number is range 1..6, please.");
            }
        }

    }

    private static void serializingDemo(Library library) throws IOException, ClassNotFoundException {

        try (FileOutputStream fileOutputStream = new FileOutputStream("yourfile.txt");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(library);
            objectOutputStream.flush();
        }

        Library serializedLibrary;
        try (FileInputStream fileInputStream = new FileInputStream("yourfile.txt");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            serializedLibrary = (Library) objectInputStream.readObject();
        }

        System.out.println(serializedLibrary.getBooks());

    }

    private static void fillLibraryWithBooks(Library library) {
        List<Book> books = List.of(
                new Book("Martin", "Clean code", 2008),
                new Book("Bulhakov", "Master and Margareta", 1950),
                new Book("Shevchenko", "Kateryna", 1850),
                new Book("Franko", "Mykyta-the-Fox", 1912),
                new Book("Ukrainka", "Contra spem spero!", 1920),
                new Book("Card", "The Ender's Game", 1990),
                new Book("Rowling", "Harry Potter", 1959),
                new Book("Card", "Star roads", 2007),
                new Book("Palamariuk", "How to make good marks?", 2020),
                new Book("Tolstoi", "War and peace", 2020),
                new Book("Gordon", "Eat my chocolate please", 2020)
        );
        books.forEach(library::addBook);
    }

    private static void fillLibraryWithSubscriptions(Library library) {
        List<LibrarySubscription> librarySubscriptions = List.of(
                new LibrarySubscription("Palamariuk", "Maksym", "palamariuk.maksym@gmail.com", new LibraryReaderInfo()),
                new LibrarySubscription("Martyniuk", "Dariia", "mardashker@gmail.com", new LibraryReaderInfo()),
                new LibrarySubscription("Lechyha", "Roman", "roma_the_best@gmail.com", new LibraryReaderInfo()),
                new LibrarySubscription("Timachov", "Daniil", "godbrowed@gmail.com", new LibraryReaderInfo())
        );

        library.getAdministrator().registerTake(library, 1, library.getBooks().get(0), librarySubscriptions.get(0), LocalDate.of(2021, 11, 11), LocalDate.of(2021, 12, 1));
        library.getAdministrator().registerTake(library, 2, library.getBooks().get(1), librarySubscriptions.get(0), LocalDate.of(2021, 11, 12), LocalDate.of(2021, 12, 3));
        library.getAdministrator().registerTake(library, 3, library.getBooks().get(2), librarySubscriptions.get(0), LocalDate.of(2021, 11, 13), LocalDate.of(2021, 12, 4));
        library.getAdministrator().registerTake(library, 3, library.getBooks().get(3), librarySubscriptions.get(0), LocalDate.of(2021, 11, 13), LocalDate.of(2021, 12, 4));
        library.getAdministrator().registerTake(library, 4, library.getBooks().get(4), librarySubscriptions.get(1), LocalDate.of(2021, 11, 14), LocalDate.of(2021, 12, 5));
        library.getAdministrator().registerTake(library, 5, library.getBooks().get(5), librarySubscriptions.get(1), LocalDate.of(2021, 11, 15), LocalDate.of(2021, 12, 6));
        library.getAdministrator().registerTake(library, 6, library.getBooks().get(6), librarySubscriptions.get(2), LocalDate.of(2021, 11, 16), LocalDate.of(2021, 12, 10));
        library.getAdministrator().registerTake(library, 7, library.getBooks().get(7), librarySubscriptions.get(3), LocalDate.of(2021, 11, 17), LocalDate.of(2021, 12, 15));
        library.getAdministrator().registerTake(library, 8, library.getBooks().get(8), librarySubscriptions.get(3), LocalDate.of(2021, 11, 18), LocalDate.of(2021, 12, 20));

        library.getAdministrator().registerReturn(library, librarySubscriptions.get(0), librarySubscriptions.get(0).getReader().getTakenBooks().get(0), LocalDate.of(2021, 12, 3));
        library.getAdministrator().registerReturn(library, librarySubscriptions.get(1), librarySubscriptions.get(1).getReader().getTakenBooks().get(0), LocalDate.of(2021, 12, 7));
        library.getAdministrator().registerReturn(library, librarySubscriptions.get(2), librarySubscriptions.get(2).getReader().getTakenBooks().get(0), LocalDate.of(2021, 12, 7));
        library.getAdministrator().registerReturn(library, librarySubscriptions.get(3), librarySubscriptions.get(3).getReader().getTakenBooks().get(0), LocalDate.of(2021, 12, 7));

        librarySubscriptions.forEach(library::addLibrarySubscription);
    }
}
