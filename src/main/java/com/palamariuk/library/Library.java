package com.palamariuk.library;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Library implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private List<Book> books;
    private List<LibrarySubscription> librarySubscriptions;
    private LibraryAdministrator administrator;

    public Library() {
        this.librarySubscriptions = new ArrayList<>();
        this.books = new ArrayList<>();
        this.administrator = new LibraryAdministrator();
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<LibrarySubscription> getLibrarySubscriptions() {
        return librarySubscriptions;
    }

    public void setLibrarySubscriptions(List<LibrarySubscription> allLibrarySubscriptions) {
        this.librarySubscriptions = allLibrarySubscriptions;
    }

    public void addBook(Book book) {
        this.books.add(book);
    }

    public void removeBook(Book book) {
        this.books.remove(book);
    }

    public void addLibrarySubscription(LibrarySubscription librarySubscription) {
        this.librarySubscriptions.add(librarySubscription);
    }

    public void removeLibrarySubscription(LibrarySubscription librarySubscription) {
        this.librarySubscriptions.remove(librarySubscription);
    }

    public LibraryAdministrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(LibraryAdministrator administrator) {
        this.administrator = administrator;
    }
}
