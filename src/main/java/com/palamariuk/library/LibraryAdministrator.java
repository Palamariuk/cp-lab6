package com.palamariuk.library;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LibraryAdministrator implements Serializable {

    @Serial
    private static final long serialVersionUID = 3L;

    private final List<LibraryRecord> records;

    public LibraryAdministrator(){
        records = new ArrayList<>();
    }

    public void registerTake(Library library, int id, Book book, LibrarySubscription librarySubscription, LocalDate takenAt, LocalDate plannedReturn) {
        if(this != library.getAdministrator()){
            return;
        }
        if (librarySubscription.getReader().getTakenBooks().size() < LibraryReaderInfo.getMaxBooks()) {
            librarySubscription.getReader().addBook(book);
//            library.removeBook(book);
            records.add(new LibraryRecord(id, book, librarySubscription, takenAt, plannedReturn));
        }
    }

    public void registerReturn(Library library, LibrarySubscription librarySubscription, Book book, LocalDate actualReturn) {
        if(this != library.getAdministrator()){
            return;
        }
        LibraryRecord libraryRecord = records.stream()
                .filter(x -> x.getBook().equals(book))
                .sorted(Comparator.comparingInt(LibraryRecord::getId))
                .toList()
                .get(0);
        libraryRecord.setActualReturnDate(actualReturn);
//        library.addBook(book);
        librarySubscription.getReader().getTakenBooks().remove(book);
    }

    public LibraryRecord getLatestByBook(Book book) {
        return records.stream()
                .filter(x -> x.getBook().equals(book))
                .sorted(Comparator.comparingInt(LibraryRecord::getId))
                .toList()
                .get(0);
    }

    public List<LibraryRecord> getRecords() {
        return records;
    }

}
