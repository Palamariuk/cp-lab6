package com.palamariuk.library;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LibraryReaderInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 4L;

    private static final int MAX_BOOKS = 5;
    private List<Book> takenBooks;

    public LibraryReaderInfo() {
        this.takenBooks = new ArrayList<Book>();
    }

    public static int getMaxBooks() {
        return MAX_BOOKS;
    }

    public List<Book> getTakenBooks() {
        return takenBooks;
    }

    public void setTakenBooks(List<Book> takenBooks) {
        this.takenBooks = takenBooks;
    }

    public void addBook(Book book) {
        this.takenBooks.add(book);
    }

    @Override
    public String toString() {
        return "Reader{" +
                "takenBooks=" + takenBooks +
                '}';
    }
}
