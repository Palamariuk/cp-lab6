package com.palamariuk.library;

import java.io.Serial;
import java.io.Serializable;

public class LibrarySubscription implements Serializable {

    @Serial
    private static final long serialVersionUID = 6L;

    private String surname;
    private String name;
    private String email;
    private LibraryReaderInfo libraryReaderInfo;

    public LibrarySubscription(String surname, String name, String email, LibraryReaderInfo libraryReaderInfo) {
        this.surname = surname;
        this.name = name;
        this.email = email;
        this.libraryReaderInfo = libraryReaderInfo;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LibraryReaderInfo getReader() {
        return libraryReaderInfo;
    }

    public void setReader(LibraryReaderInfo libraryReaderInfo) {
        this.libraryReaderInfo = libraryReaderInfo;
    }

    @Override
    public String toString() {
        return "LibrarySubscription{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", reader=" + libraryReaderInfo +
                '}';
    }
}
