package com.palamariuk.library;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

public class LibraryRecord implements Serializable {

    @Serial
    private static final long serialVersionUID = 5L;

    private int id;
    private Book book;
    private LibrarySubscription librarySubscription;
    private LocalDate takingDate;
    private LocalDate plannedReturnDate;
    private LocalDate actualReturnDate;

    public LibraryRecord(int id, Book book, LibrarySubscription librarySubscription, LocalDate takingDate, LocalDate plannedReturnDate) {
        this.id = id;
        this.book = book;
        this.librarySubscription = librarySubscription;
        this.takingDate = takingDate;
        this.plannedReturnDate = plannedReturnDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LocalDate getTakingDate() {
        return takingDate;
    }

    public void setTakingDate(LocalDate takingDate) {
        this.takingDate = takingDate;
    }

    public LocalDate getPlannedReturnDate() {
        return plannedReturnDate;
    }

    public void setPlannedReturnDate(LocalDate plannedReturnDate) {
        this.plannedReturnDate = plannedReturnDate;
    }

    public LocalDate getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(LocalDate actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    public LibrarySubscription getLibrarySubscription() {
        return librarySubscription;
    }

    public void setLibrarySubscription(LibrarySubscription librarySubscription) {
        this.librarySubscription = librarySubscription;
    }
}
